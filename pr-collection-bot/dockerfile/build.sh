set -e
docker build --progress=plain --no-cache --ssh bitbucket=~/.ssh/id_rsa -t 968187346014.dkr.ecr.cn-northwest-1.amazonaws.com.cn/pr-collection-bot:0.1.1 .
docker push 968187346014.dkr.ecr.cn-northwest-1.amazonaws.com.cn/pr-collection-bot:0.1.1