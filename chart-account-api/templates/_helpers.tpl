{{/* Generate basic lables */}}
{{- define "account-api.labels" }}
labels:
  App: {{ .Chart.Name }}-{{ .Values.env }}
  Env: {{ .Values.node_env }}
  Service: {{ .Values.service_type }}
{{- end }}

{{/* Generate basic strategy */}}
{{- define "account-api.strategy" }}
strategy:
  rollingUpdate:
    maxSurge: {{ .Values.strategy.rollingUpdate.maxSurge }}
    maxUnavailable: {{ .Values.strategy.rollingUpdate.maxUnavailable }}
  type: {{ .Values.strategy.type }}
{{- end }}

{{/* Generate aws ingress annotations */}}
{{- define "account-api.aws.ingress.annotations" }}
{{- with .Values.annotations }}
annotations:
  kubernetes.io/ingress.class: {{ .kubernetes_io.aws.ingress_class}}
  alb.ingress.kubernetes.io/scheme: {{ .alb_ingress_kubernetes_io.scheme }}
  alb.ingress.kubernetes.io/load-balancer-name: {{ .alb_ingress_kubernetes_io.load_balancer_name }}
  alb.ingress.kubernetes.io/group.name: {{ .alb_ingress_kubernetes_io.group_name }}
  alb.ingress.kubernetes.io/listen-ports: {{ .alb_ingress_kubernetes_io.listen_ports }}
  alb.ingress.kubernetes.io/target-type: {{ .alb_ingress_kubernetes_io.target_type }}
  alb.ingress.kubernetes.io/healthcheck-port: {{ $.Values.health.port | squote }}
  alb.ingress.kubernetes.io/healthcheck-path: {{ $.Values.health.path }}
  alb.ingress.kubernetes.io/load-balancer-attributes: {{ .alb_ingress_kubernetes_io.load_balancer_attributes }}
{{- end }}
{{- end }}